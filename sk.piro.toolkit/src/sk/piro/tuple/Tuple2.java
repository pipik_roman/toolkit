package sk.piro.tuple;

import java.util.Iterator;

/**
 * Tuple holding two objects. For efficiency we do not use array or collection here
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 * @param <Type1> type of first object
 * @param <Type2> type of second object
 */
public class Tuple2<Type1,Type2> extends Tuple<Object>{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Create {@link Tuple2} from iterable. If iterable has more than 2 elements only first two are taken. If iterable has less then 2 elements they are null
	 * @param iterable iterable to convert to tuple
	 * @return tuple containing first two elements from iterable
	 */
	public static final <Type> Tuple2<Type, Type> from(Iterable<Type> iterable){
		Type _1 = null;
		Type _2 = null;
		int index = 0;
		t1:for(Type t : iterable) {
			switch (index++) {
			case 0: _1 = t; break;
			case 1: _2 = t; break;
			default: break t1; //I am haxor
			}
		}
		return new Tuple2<Type, Type>(_1, _2);
	}
	
	/**
	 * Create {@link Tuple2} from iterable. If iterable has more than 2 elements only first two are taken. If iterable has less then 2 elements they are null
	 * @param iterable iterable to convert to tuple
	 * @return tuple containing first two elements from iterable
	 */
	public static final <Type> Tuple2<Type, Type> from(Type[] array){
		Type _1 = null;
		Type _2 = null;
		for(int index = 0; index < array.length && index < 2; index++) {
			switch (index) {
			case 0: _1 = array[index]; break;
			case 1: _2 = array[index]; break;
			default: throw new AssertionError("Reading third element from iterable shoould never happen!");
			}
		}
		return new Tuple2<Type, Type>(_1, _2);
	}
	
	public final Type1 _1;
	public final Type2 _2;
	
	public Tuple2(Type1 _1, Type2 _2) {
		super();
		this._1 = _1;
		this._2 = _2;
	}

	@Override
	public Iterator<Object> iterator() {
		return new Iterator<Object>() {
			private int index = 0;
			@Override
			public boolean hasNext() {
				return index < 2; 
			}

			@Override
			public Object next() {
				switch (index++) {
				case 0: return _1;
				case 1: return _2;
				default:
					throw new IllegalStateException("No more elements");
				}
			}
		};
	}

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public Object get(int index) {
		switch (index) {
		case 0: return _1;
		case 1: return _2;
		default: throw new IndexOutOfBoundsException("Index " + index + " is invalid since size of tuple is " + getSize());
		}
	}
}
