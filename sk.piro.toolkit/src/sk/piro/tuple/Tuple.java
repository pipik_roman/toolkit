package sk.piro.tuple;

import java.io.Serializable;

/**
 * Tuple interface. For tuple we want to know how many elements it holds and want to iterate over them. 
 * But since tuple objects can be of different type we do not want to specify object types. But in future we might add support for iteration with common type
 * After I started with tuples I found http://www.javatuples.org/download.html but I instead stick to naming Tuple2,3,..... like in Scala.
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 */
public abstract class Tuple<CommonType> implements Iterable<CommonType>, Serializable{
	private static final long serialVersionUID = 1L;
	public abstract int getSize();
	public abstract CommonType get(int index);
}
