package sk.piro.toolkit;

import java.util.HashMap;
import java.util.Map;

/**
 * Utilities for maps
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 */
public class MapUtils {

	/**
	 * Get value from map. Value has to be {@link Map}. If not value exists for given key create new {@link HashMap} return it and insert it into parent map. 
	 * @param map map to read value from
	 * @param key key for value
	 * @return map for given key or newly created {@link HashMap} inserted into map
	 */
	public static <KeyType, SubMapValueKeyType, SubMapValueType> Map<SubMapValueKeyType, SubMapValueType> getOrInsertHashMap(Map<KeyType, Map<SubMapValueKeyType, SubMapValueType>> map, KeyType key) {
		Map<SubMapValueKeyType, SubMapValueType> subMap = map.get(key);
		if(subMap == null) {
			subMap = new HashMap<SubMapValueKeyType, SubMapValueType>();
			map.put(key, subMap);
		}
		return subMap;
	}
	
	/**
	 * Get value from map. If no value exists for given key return defaultValue and add defaultValue to map;
	 * @param map map to read value from
	 * @param key key for value
	 * @param defaultToInsert default value to return and insert in case no value is found for key
	 * @return
	 */
	public static <KeyType, ValueType> ValueType getOrInsert(Map<KeyType, ValueType> map, KeyType key, ValueType defaultToInsert) {
		ValueType value = map.get(key);
		if(value == null) {
			value = defaultToInsert;
			map.put(key, value);
		}
		return value;
	}
	
	/**
	 * Returns value from map for specified key or returns default value when there is no mapping for key
	 * @param map
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public static <KeyType, ValueType> ValueType get(Map<KeyType,ValueType> map, KeyType key, ValueType defaultValue) {
		ValueType value = map.get(key);
		if(value == null) {
			value = defaultValue;
		}
		return value;
	}
	
	/**
	 * Increment value in map for specified key. Used for counting values by key
	 * @param map
	 * @param key
	 */
	public static <KeyType> void increment(Map<KeyType, Integer> map, KeyType key) {
		Integer count = map.get(key);
		if(count == null) {
			count = 0;
		}
		map.put(key, count + 1);
	}
}
