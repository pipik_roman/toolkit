package sk.piro.toolkit;

public abstract class Format<ValueType> {

	private static final String[] byteSizes = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
	private static final String[] biByteSizes = { "B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB" };
	private static final String[] secondTimes = { "ns", "us", "ms", "s" };

	/**
	 * Invokes {@link #adaptUnit(long, int, Object[])} for byte sizes. Using decimal scale
	 * 
	 * @param bytes
	 *            size in bytes
	 * @return best fitting size
	 */
	public static ValueWithUnit<Long, String> formatByteSize(long bytes) {
		return adaptUnit(bytes, 0, byteSizes);
	}
	
	/**
	 * Invokes {@link #adaptUnit(long, int, Object[])} for byte sizes. Using decimal scale
	 * 
	 * @param bytes
	 *            size in bytes
	 * @return best fitting size
	 */
	public static ValueWithUnit<Long, String> formatBiByteSize(long bytes) {
		return nextWhile(bytes, 1024,0, biByteSizes);
	}
	
	/**
	 * Invokes {@link #adaptUnit(long, int, Object[])} for time in nanoseconds. Using decimal scale. Useful for formatting {@link System#nanoTime()} results
	 * 
	 * @param nanoseconds
	 *            nanoseconds
	 * @return best fitting size
	 */
	public static ValueWithUnit<Long, String> formatNanoseconds(long nanoseconds) {
		return adaptUnit(nanoseconds, 0, secondTimes);
	}

	/**
	 * Try to choose unit for which value is < 1000
	 * 
	 * @param value
	 *            value to adapt
	 * @param actualUnit
	 *            actual index of unit of value
	 * @param units
	 *            units available
	 * @return value with adapted unit
	 */
	public static <Unit> ValueWithUnit<Long, Unit> adaptUnit(long value, int actualUnit, Unit[] units) {
		return nextWhile(value, 1000, actualUnit, units);
	}

	private static <Unit> ValueWithUnit<Long, Unit> nextWhile(long value, int maxValue, int index, Unit[] units) {
		if (index == units.length - 1 || value < maxValue) {
			return new ValueWithUnit<Long, Unit>(value, units[index]);
		} else {
			return nextWhile(value / maxValue, maxValue, ++index, units);
		}
	}
}
