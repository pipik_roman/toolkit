package sk.piro.toolkit;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Guard object preventing its parent from serialization with reasonable message.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class NotSerializable implements Externalizable {
	private final Object parent;

	/**
	 * Create non serializable guard of object. If parent object has reference (not transient) to this guard it will not be serialized and exception will be thrown
	 * 
	 * @param parent
	 *            object to not be serializable
	 */
	public NotSerializable(Object parent) {
		super();
		this.parent = parent;
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		throw new AssertionError("This object prevents serialization so this method cannot be invoked normally");
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		throw new AssertionError("Object: " + parent.getClass() + ": " + parent + " is not serializable. This is thrown by non serializable guard");
	}
}
