package sk.piro.toolkit;


/**
 * Interface for cloneable objects
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <R>
 *            real type of object
 */
public interface Cloneable<R> extends java.lang.Cloneable{

	/**
	 * This will return clone of object. 
	 * @return clone of object
	 */
	public R clone();
}
