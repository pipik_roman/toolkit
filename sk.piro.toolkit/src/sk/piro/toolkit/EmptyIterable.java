package sk.piro.toolkit;

import java.util.Iterator;

/**
 * Implementation of iterable that has no elements
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Type>
 *            type of iterable elements
 */
public class EmptyIterable<Type> implements Iterable<Type> {
	private final  EmptyIterator<Type> iterator = new EmptyIterator<Type>();
	@Override
	public Iterator<Type> iterator() {
		return iterator;
	}
}
