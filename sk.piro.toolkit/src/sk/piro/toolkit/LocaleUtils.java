package sk.piro.toolkit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Locale utilities. Resolves some localization issues. Mostly in Slovak environment
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 */
public class LocaleUtils {
	private static final Map<String, Set<String>> languageToTrueValues = new HashMap<String, Set<String>>();
	
	/**
	 * Unspecified language constant. Represents also constants common for all languages
	 */
	public static final String LANGUAGE_NONE = "";
	/**
	 * Slovak language constant by ISO
	 */
	public static final String LANGUAGE_SK = "sk";
	/**
	 * English language constant by ISO
	 */
	public static final String LANGUAGE_EN = "en";
	
	static {
		addTrueValues(LANGUAGE_NONE, "t", "1", "true");
		addTrueValues(LANGUAGE_SK, "a","ano");
		addTrueValues(LANGUAGE_EN, "y","yes");
	}
	
	/**
	 * Add String values representing boolean true for language code 
	 * @param languageCode language code
	 * @param trueValues String values representing boolean true for language code
	 */
	public static final void addTrueValues(String languageCode, String ... trueValues) {
		Set<String > trueValuesSet = languageToTrueValues.get(languageCode);
		if(trueValuesSet == null) {
			trueValuesSet = new HashSet<String>();
			languageToTrueValues.put(languageCode, trueValuesSet);
		}
		for(String trueValue : trueValues) {
			trueValuesSet.add(trueValue);
		}
	}
	
	/**
	 * Return list of String values representing boolean true value for language code. Values are lower case 
	 * @param languageCode language code 
	 * @return list of String values representing boolean true value for language code
	 */
	public static final Set<String> getTrueValues(String languageCode){
		Set<String> trueValues = languageToTrueValues.get(languageCode);
		if(trueValues == null) {
			trueValues = new HashSet<String>();
		}
		return trueValues;
	}
	
}
