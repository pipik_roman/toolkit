package sk.piro.toolkit;
import java.util.Iterator;

/**
 * Empty iterator
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 * @param <Type>
 */
public class EmptyIterator<Type> implements Iterator<Type> {

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public Type next() {
		throw new AssertionError("This iterator represents non-iterable object and this method should be never called (hasNext() returned false)");
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot remove any elements from non-iterable object");
	}
}
