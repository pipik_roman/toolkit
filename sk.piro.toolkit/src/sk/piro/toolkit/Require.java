package sk.piro.toolkit;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * Toolkit with basic requirements. Throws {@link IllegalStateException}, {@link IllegalArgumentException}, {@link NullPointerException} depending on use case with helpful description
 * 
 * @author Ing. Pipik Roman - roman.pipik@dominanz.sk pipik.roman@gmail.com www.dominanz.sk
 * 
 */
public class Require {
	public static final String NPE = NullPointerException.class.getCanonicalName();
	public static final String AE = AssertionError.class.getCanonicalName();
	public static final String IAE = IllegalArgumentException.class.getCanonicalName();
	public static final String ISE = IllegalStateException.class.getCanonicalName();
	/**
	 * Throws {@link IllegalStateException} if requirement is not valid
	 * 
	 * @param valid
	 *            state requirement
	 * @param description
	 *            description about state requirement
	 * @throws IllegalStateException
	 *             if requirement is not valid
	 */
	public static final void requireState(boolean valid, Object ... description) throws IllegalStateException {
		require(valid, null, null, "is in invalid state", ISE, description);
	}

	/**
	 * Throws {@link IllegalArgumentException} if requirement is not true
	 * 
	 * @param valid
	 *            true if requirement is valid
	 * @param field
	 *            value of requirement tested
	 * @throws IllegalArgumentException
	 *             if requirement is not valid
	 */
	public static final <Type> Type require(boolean valid, Type field) throws IllegalArgumentException {
		return require(valid, field, null);
	}

	/**
	 * Throws {@link IllegalArgumentException} if requirement is not true
	 * 
	 * @param valid
	 *            true if requirement is valid
	 * @param field
	 *            value of requirement tested
	 * @param fieldName
	 *            field name of value
	 * @throws IllegalArgumentException
	 *             if requirement is not valid
	 */
	public static final <Type> Type require(boolean valid, Type field, String fieldName) throws IllegalArgumentException {
		return require(valid, field, fieldName,  (Object[]) null);
	}

	/**
	 * Throws {@link IllegalArgumentException} if requirement is not true
	 * 
	 * @param valid
	 *            true if requirement is valid
	 * @param field
	 *            value of requirement tested
	 * @param fieldName
	 *            field name of value
	 * @param description
	 *            description for requirement - multiple objects will be appended. This prevents unwanted string creations when requirement is met!
	 * @throws IllegalArgumentException
	 *             if requirement is not valid
	 */
	public static final <Type> Type require(boolean valid, Type field, String fieldName, Object ... description) throws IllegalArgumentException {
		return require(valid, field, fieldName, "has invalid value", null, description);
	}
	
	/**
	 * Base implementation of test. Throws {@link IllegalArgumentException} if requirement is not true
	 * 
	 * @param valid
	 *            true if requirement is valid
	 * @param field
	 *            value of requirement tested
	 * @param fieldName
	 *            field name of value
	 * @param cause 
	 * 			  cause of exception inserted into error message. Examples "has invalid value", ...
	 * @param exceptionClass
	 * 		  class of exception to be thrown. For now we do not support use of reflection (to keep this API available on platforms where reflection is problem (GWT). Default value is null 
	 * @param description
	 *            description for requirement - multiple objects will be appended. This prevents unwanted string creations when requirement is met!
	 * @throws IllegalArgumentException by default or other exception defined in exceptionClass
	 */
	private static final <Type> Type require(boolean valid, Type field, String fieldName, String cause, String exceptionClass, Object ... description) throws IllegalArgumentException {
		if (valid) {
			return field;
		}
		
		String msg = createMessage(fieldName, field,cause, description);
		
		if(exceptionClass == null || IAE.equals(exceptionClass)) {// most common case
			throw new IllegalArgumentException(msg);
		}
		if(NPE.equals(exceptionClass)) {
			throw new NullPointerException(msg);
		}
		if(ISE.equals(exceptionClass)) {
			throw new IllegalStateException(msg);
		}
		if(AE.equals(exceptionClass)) {
			throw new AssertionError(msg);
		}
		throw new IllegalArgumentException("Custom exception class '" + exceptionClass + "' is not supported by " + Require.class.getName()+".require  method! Currently we support only basic exception types (NPE, IAE, AE, ISE)");
	}

	/**
	 * This will build error message about invalid value
	 * @param field name of field tested
	 * @param value value of field tested
	 * @param cause cause of error - this expects "has invalid value" or "is empty" ... so we can reuse error message creation logic
	 * @param description detailed information about failed test. Split into separate objects to prevent preemptive resolution (unnecessary creation of strings)
	 * @return invalid value message
	 */
	private static final String createMessage(String field, Object value, String cause, Object ... description) {
		final StringBuilder sb = new StringBuilder();
		
		if (field == null) {
			sb.append("Field");
		} else {
			sb.append("Field '");
			sb.append(field);
			sb.append("'");
		}
		sb.append(" of type '");
		if (value == null) {
			sb.append("?");
		} else {
			sb.append(value.getClass().getName());
		}
		sb.append("' ");
		sb.append(cause);
		sb.append(" '");
		if (value == null) {
			sb.append("null");
		} else {
			sb.append(value.toString());
		}	
		sb.append("'!");
		if (description != null && description.length > 0) {
			sb.append(" ");
			for(Object s : description) {
				sb.append(s);
			}
		}

		return sb.toString();
	}

	/**
	 * Test object for null
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @throws IllegalArgumentException
	 *             if object is null
	 */
	public static <Type> Type isNotNull(Type object, String fieldName) throws IllegalArgumentException {
		return isNotNull(object, fieldName, "");
	}

	/**
	 * Test object for null
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @param description
	 *            description. For example why object cannot be null
	 * @throws IllegalArgumentException
	 *             if object is null
	 */
	public static <Type> Type isNotNull(Type object, String fieldName, Object ... description) throws IllegalArgumentException {
		return require(object != null, object, fieldName, "is", null, description); // mesasge will be is 'null'
	}

	/**
	 * Test char sequence for null or emptiness
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @throws IllegalArgumentException
	 *             if object is null or empty
	 */
	public static <Type extends CharSequence> Type isNotNullOrEmpty(Type object, String fieldName) throws IllegalArgumentException {
		return isNotNullOrEmpty(object, fieldName, fieldName + " cannot be null or empty");
	}

	/**
	 * Test object for null or emptiness
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @param description
	 *            description. For example why object cannot be null
	 * @throws IllegalArgumentException
	 *             if object is null or empty
	 */
	public static <Type extends CharSequence> Type isNotNullOrEmpty(Type object, String fieldName, Object ... description) throws IllegalArgumentException {
		isNotNull(object, fieldName, description);
		require(object.length() > 0, object, fieldName,"is empty", null, description);
		return object;
	}

	/**
	 * Test collection for null or emptiness
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @throws IllegalArgumentException
	 *             if object is null or empty
	 */
	public static <Type extends Collection<?>> Type isNotNullOrEmpty(Type object, String fieldName) throws IllegalArgumentException {
		return isNotNullOrEmpty(object, fieldName, fieldName + " cannot be null or empty");
	}
	
	/**
	 * Test collection for null or emptiness
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @param description
	 *            description. For example why object cannot be null
	 * @throws IllegalArgumentException
	 *             if object is null or empty
	 */
	public static <Type extends Collection<?>> Type isNotNullOrEmpty(Type object, String fieldName, Object ... description) throws IllegalArgumentException {
		isNotNull(object, fieldName, description);
		require(object.size() > 0, object, fieldName, null, description);
		return object;
	}
	
	/**
	 * Test map for null or emptiness. When map contains at least one key it is considered not empty even when value is null!
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @throws IllegalArgumentException
	 *             if object is null or empty
	 */
	public static <Type extends Map<?,?>> Type isNotNullOrEmpty(Type object, String fieldName) throws IllegalArgumentException {
		return isNotNullOrEmpty(object, fieldName, fieldName + " cannot be null or empty");
	}

	/**
	 * Test map for null or emptiness. When map contains at least one key it is considered not empty even when value is null!
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @param description
	 *            description. For example why object cannot be null
	 * @throws IllegalArgumentException
	 *             if object is null or empty
	 */
	public static <Type extends Map<?,?>> Type isNotNullOrEmpty(Type object, String fieldName, Object ... description) throws IllegalArgumentException {
		isNotNull(object, fieldName, description);
		require(object.size() > 0, object, fieldName, null, description);
		return object;
	}

	/**
	 * Test object for null
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @throws IllegalStateException
	 *             if object is NOT null
	 */
	public static <Type> Type isNullState(Type object, String fieldName) throws IllegalStateException {
		return isNullState(object, fieldName, "");
	}

	/**
	 * Test object for null
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @param description
	 *            description. For example why object must null
	 * @throws IllegalStateException
	 *             if object is NOT null
	 */
	public static <Type> Type isNullState(Type object, String fieldName, Object ... description) throws IllegalStateException {
		return require(object == null, object, fieldName, "is not null", ISE, description);
	}

	/**
	 * Test object for null
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @throws IllegalStateException
	 *             if object is null
	 */
	public static <Type> Type isNotNullState(Type object, String fieldName) throws IllegalStateException {
		return isNotNullState(object, fieldName, "");
	}

	/**
	 * Test object for null
	 * 
	 * @param object
	 *            object to test
	 * @param fieldName
	 *            name of object field
	 * @param description
	 *            description. For example why object must null
	 * @throws IllegalStateException
	 *             if object is null
	 */
	public static <Type> Type isNotNullState(Type object, String fieldName, Object ... description) throws IllegalStateException {
		return require(object != null, object, fieldName, "is", ISE, description); // message will be is 'null' where null comes from object
	}

	/**
	 * Almost like {@link #isNotNullOrEmpty(Collection, String, Object...)} but does not throw for null collection, only if it is empty!
	 * @param collection collection that cannot be empty when not null!
	 * @param fieldName field name 
	 * @param description error description parts
	 * @throws IllegalArgumentException if collection is not null but is empty
	 * @return
	 */
	public static <Type extends Collection<?>> Type isNotEmpty(Type collection, String fieldName, Object ... description) throws IllegalArgumentException{
		return require(collection == null || !collection.isEmpty(), collection, fieldName, "is empty", IAE, description);
	}
	
	/**
	 * Almost like {@link #isNotNullOrEmpty(Map, String, Object...)} but does not throw for null map, only if it is empty!
	 * @param map map that cannot be empty when not null!
	 * @param fieldName field name 
	 * @param description error description parts
	 * @throws IllegalArgumentException if map is not null but is empty
	 * @return
	 */
	public static <KeyType, ValueType> Map<KeyType, ValueType>isNotEmpty(Map<KeyType,ValueType> map, String fieldName, Object ... description) throws IllegalArgumentException{
		return require(map == null || !map.isEmpty(), map, fieldName, "is empty", IAE, description);
	}

	public static <KeyType, ValueType> ValueType get(Map<KeyType, ValueType> map, KeyType key) {
		return get(map, key, null);
	}

	public static <KeyType, ValueType> ValueType get(Map<KeyType, ValueType> map, KeyType key, String objectName) {
		ValueType value = map.get(key);
		if (value != null) {
			return value;
		}

		final String objectNameString;
		if (objectName == null) {
			objectNameString = "";
		} else {
			objectNameString = objectName + " ";
		}

		if (key == null) {
			throw new IllegalArgumentException(objectNameString + "not found for key '" + key + "' in map. Map keys: " + map.keySet());
		} else {
			throw new IllegalArgumentException(objectNameString + "not found for key '" + key + "' in map. Key type: " + key.getClass().getName() + "Map keys: " + map.keySet());
		}
	}

	public static <KeyType, ValueType> ValueType getOrElse(Map<KeyType, ValueType> map, KeyType key, ValueType defaultValue) {
		ValueType value = map.get(key);
		if (value != null) {
			return value;
		} else {
			return defaultValue;
		}
	}

	public static <KeyType, ValueType> ValueType getOrElseUpdate(Map<KeyType, ValueType> map, KeyType key, ValueType defaultValue) {
		ValueType value = map.get(key);
		if (value != null) {
			return value;
		} else {
			map.put(key, defaultValue);
			return defaultValue;
		}
	}

	public static <KeyType, ValueType> void add(Map<KeyType, ValueType> map, KeyType key, ValueType value) {
		add(map, key, value, "");
	}

	public static <KeyType, ValueType> void add(Map<KeyType, ValueType> map, KeyType key, ValueType value, String description) {
		if (!map.containsKey(key)) {
			map.put(key, value);
			return;
		}
		throw new IllegalArgumentException("Map already contains value for key : " + key + "! Cannot add value! " + description);
	}

	public static <ElementType> void add(Collection<ElementType> collection, ElementType element) {
		add(collection, element, "");
	}

	public static <ElementType> void add(Collection<ElementType> collection, ElementType element, String description) {
		if (!collection.contains(element)) {
			collection.add(element);
			return;
		}
		throw new IllegalArgumentException("Collection already contains element : " + element + "! Cannot add value " + description);
	}
	
	/**
	 * Throws {@link IllegalArgumentException} if objects are not equal
	 * 
	 * @param expected expected value of field 
	 * @param real
	 *            real value of field
	 * @param fieldName name of field           
	 * @throws IllegalArgumentException
	 *             if objects are not equal
	 */
	public static final <T> T isSame(T expected, T real, String fieldName) throws IllegalArgumentException {
		return require((expected == null && real == null) || (expected != null && expected.equals(real)) , real, fieldName, "is not same as expected value '" + expected + "'", null); 
	}
	
	/**
	 * Throws {@link IllegalArgumentException} if objects are not equal
	 * 
	 * @param expected expected value of field 
	 * @param real
	 *            real value of field           
	 * @throws IllegalArgumentException
	 *             if objects are not equal
	 */
	public static final <T> T isSame(T expected, T real) throws IllegalArgumentException {
		return isSame(expected, real, null);
	}
	
	/**
	 * Throws {@link IllegalArgumentException} if object is not equal to one of expected
	 * 
	 *  
	 * @param real
	 *            real value of field
	 * @param fieldName name of field
	 * @param expected array of expected values           
	 * @throws IllegalArgumentException
	 *             if object is not equal to one of expected
	 */
	public static final <T> T isOneOf(T real, String fieldName,T ... expected) throws IllegalArgumentException {
		return require(isOneOfCompare(real, expected), real, fieldName, " is not one of expected values " + Arrays.toString(expected)+ ", but is", null);
	}
	
	private static <T> boolean isOneOfCompare(T real, T[] expected) {
		for(T expectedOne : expected) {
			if(expectedOne == null ) {
				if(real == null) {
					return true;
				}
			}
			else {
				if(expectedOne.equals(real)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Throws {@link IllegalArgumentException} if object is not equal to one of expected
	 * 
	 *  
	 * @param real
	 *            real value of field
	 * @param expected array of expected values           
	 * @throws IllegalArgumentException
	 *             if object is not equal to one of expected
	 */
	public static final <T> void  isOneOf(T real,T ... expected) throws IllegalArgumentException {
		isOneOf(real, null,expected);
	}
}
