package sk.piro.toolkit;

import java.io.Serializable;

public class ValueWithUnit<Value, Unit> implements Serializable {
	private static final long serialVersionUID = 8777509936927585577L;
	private Value value;
	private Unit unit;

	private ValueWithUnit() {
		// for GWT serialization
	}

	public ValueWithUnit(Value value, Unit unit) {
		this();
		this.value = value;
		this.unit = unit;
	}

	public Unit getUnit() {
		return unit;
	}

	public Value getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value.toString() + unit.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueWithUnit<?, ?> other = (ValueWithUnit<?, ?>) obj;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
