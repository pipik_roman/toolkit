package sk.piro.toolkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Utilities for string operations
 * 
 * @author Pipik Roman - pipik.roman@gmail.com roman.pipik@dominanz.sk
 * 
 */
public class StringUtils {
	/**
	 * Replace characters in String. There is no suitable operation in String or StringBuilder except regex replacement
	 * 
	 * @param original
	 *            original string
	 * @param charsToFind
	 *            list of characters to find
	 * @param replacement
	 *            list of characters to replace. Can be same length as charsToFind or length 1
	 * @return string with specified characters replaced with replacements
	 */
	public static String replaceAllChars(String original, char[] charsToFind, char[] replacement) {
		StringBuilder sb = new StringBuilder(original);
		if (replacement.length == 1) {
			char c = replacement[0];
			replacement = new char[charsToFind.length];
			Arrays.fill(replacement, c);
		}
		for (int i = 0; i < charsToFind.length; i++) {
			int index = 0;
			String stringToFind = String.valueOf(charsToFind[i]);
			String stringReplacement = String.valueOf(replacement[i]);
			while ((index = sb.indexOf(stringToFind, index)) != -1) {
				sb.replace(index, index + 1, stringReplacement);
			}
		}
		return sb.toString();
	}
	
	public static String trimRight(String string) {
		char[] value = string.toCharArray();
        int len = value.length;

        while ((0 < len) && (value[len - 1] <= ' ')) {
            len--;
        }
        return (len < value.length) ? string.substring(0, len) : string;
    }
	
	public static String trimLeft(String string) {
		char[] value = string.toCharArray();
        int st = 0;
        while ((st < value.length) && (value[st] <= ' ')) {
            st++;
        }
        return (st > 0) ? string.substring(st) : string;
    }


	/**
	 * Replace characters in String. There is no suitable operation in String or StringBuilder except regex replacement
	 * 
	 * @param original
	 *            original string
	 * @param charsToFind
	 *            list of characters to find
	 * @param replacement
	 *            character to use as replacement for all charsToFind
	 * @return string with specified characters replaced with replacements
	 */
	public static String replaceAllChars(String original, char replacement, char... charsToFind) {
		return replaceAllChars(original, charsToFind, new char[] { replacement });
	}
	
	/**
	 * Create string by repeating characters n times
	 * @param times times to repeat characters
	 * @param chars characters to repeat
	 * @return string created by repeating characters specified times
	 */
	public static String repeat(int times, char ... chars) {
		StringBuilder sb = new StringBuilder(times* chars.length);
		for(int i = 0;i < times; i++) {
			sb.append(chars);
		}
		return sb.toString();
	}

	/**
	 * Advanced boolean value parsing. Returns true when logical meaning of text is true, like 'true','t' and '1' are returning true. Other values can mean true
	 * if defined by locale
	 * 
	 * @param text
	 *            text to parse, case insensitive
	 * @param languageCodes
	 *            variable array of language codes used to recognize true values. Language related issues are defined in {@link LocaleUtils}
	 * @return true when logical meaning of text is true, like 'true','yes' and '1' are returning true. Otherwise returns false
	 */
	public static final boolean parseBoolean(String text, String... languageCodes) {
		if (languageCodes.length == 0 || (!languageCodes[0].equals(LocaleUtils.LANGUAGE_NONE))) {
			if (parseBoolean(text, LocaleUtils.LANGUAGE_NONE)) {
				return true;
			}
		}
		text = text.trim().toLowerCase();
		for (String languageCode : languageCodes) {
			Set<String> trueValues = LocaleUtils.getTrueValues(languageCode);
			if (trueValues.contains(text)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Prepends zeros to specified length
	 * 
	 * @param string string to prepend zeros before
	 * @param length
	 *            length
	 * @return number with prepended zeros
	 */
	public static String prependZeros(String string, int length) {
		return prependChar(string,'0', length);
	}
	
	/**
	 * Prepends zeros to specified length
	 * 
	 * @param number
	 *            number to prepend zeros before
	 * @param length
	 *            final length
	 * @return number with prepended zeros
	 */
	public static String prependZeros(long number, int length) {
		return prependChar(Long.toString(number),'0', length);
	}

	/**
	 * Prepends zeros to specified length
	 * @param number number to prepend zeros before
	 * @param length final length
	 * @return
	 */
	public static String prependZeros(int number, int length) {
		return prependChar(Integer.toString(number),'0', length);
	}
	
	/**
	 * Prepend specified character while string is shorter than specified. 
	 * @param input initial string
	 * @param c character to prepend
	 * @param length final length of string
	 * @return String with prepended characters. If initial String is same or londer as final length, initial String is returned
	 */
	public static String prependChar(String input, char c, int length) {
		if(input.length() >= length) {
			return input;
		}
		StringBuilder sb = new StringBuilder(length);
		sb.append(input);
		prependChar(sb, c, length);
		return sb.toString();
	}
	
	/**
	 * Prepend specified character while string is shorter than specified. StringBuilder version 
	 * @param builder builder to use for string
	 * @param c character to prepend
	 * @param length final length
	 */
	public static void prependChar(StringBuilder builder, char c,int length) {
		while(builder.length() < length) {
			builder.insert(0, c);
		}
	}

	/**
	 * Split string by comma and create list of it
	 * 
	 * @param list
	 *            list of values as string
	 * @return list of values as {@link ArrayList}
	 */
	public static final List<String> csvList(String listString) {
		List<String> list = new ArrayList<String>();
		for (String s : listString.split(",")) {
			s = s.trim();
			if (s.isEmpty()) {
				continue;
			}
			list.add(s);
		}
		return list;
	}

	/**
	 * Split string by comma and create set of it
	 * 
	 * @param set
	 *            set of values as string
	 * @return set of values as {@link LinkedHashSet}
	 */
	public static final Set<String> csvSet(String setString) {
		Set<String> set = new LinkedHashSet<String>();
		for (String s : setString.split(",")) {
			s = s.trim();
			if (s.isEmpty()) {
				continue;
			}
			set.add(s);
		}
		return set;
	}
	
	/**
	 * Split string by comma and create array of it
	 * 
	 * @param set
	 *            set of values as string
	 * @return set of values as array
	 */
	public static final String[] csvArray(String setString) {
		List<String> list = new ArrayList<String>();
		for (String s : setString.split(",")) {
			s = s.trim();
			if (s.isEmpty()) {
				continue;
			}
			list.add(s);
		}
		return list.toArray(new String[list.size()]);
	}

	/**
	 * Split string by comma and add to existing list
	 * 
	 * @param list
	 *            list of values as string
	 */
	public static final void csvList(String listString, List<String> list) {
		for (String s : listString.split(",")) {
			s = s.trim();
			if (s.isEmpty()) {
				continue;
			}
			list.add(s);
		}
	}

	/**
	 * Split string by comma and add to existing set
	 * 
	 * @param set
	 *            set of values as string
	 */
	public static final void csvSet(String setString, Set<String> set) {
		for (String s : setString.split(",")) {
			s = s.trim();
			if (s.isEmpty()) {
				continue;
			}
			set.add(s);
		}
	}
	
	/**
	 * Convert map to CSV
	 * @param map map to convert to csv
	 */
	public static final String csvString(Map<?, ?> map){
		return csvString(map.entrySet());
	}
	
	public static final String csvString(Iterable<?> iterable){
		StringBuilder sb = new StringBuilder();
		for(Object e : iterable){
			sb.append(e.toString());
			sb.append(",");
		}
		sb.replace(sb.length() -1, sb.length(), "");
		return sb.toString();
	}
	
	

	/**
	 * IndexOf method. Instead of String indexOf this can be set to be case insensitive! No more converting whole String to different case
	 * 
	 * @param context
	 *            text to search in
	 * @param caseSensitive
	 *            true if search is case sensitive, false if search is case insensitive
	 * @param from
	 *            beginning index of search inclusive
	 * @param toFind
	 *            string to find
	 * @return starting index of string toFind in context
	 */
	public static int indexOf(String context, boolean caseSensitive, int from, String toFind) {
		int toFindIndex = 0;
		char l = toFind.charAt(toFindIndex);// char to match now
		if (!caseSensitive) {
			l = Character.toLowerCase(l);
		}
		for (int i = from; i < context.length(); i++) {
			char c = context.charAt(i);
			if (!caseSensitive) {
				c = Character.toLowerCase(c);
			}
			if (c != l) {
				toFindIndex = 0;
			} else if (toFindIndex + 1 == toFind.length()) {
				return i - toFind.length() + 1;
				// all characters have been found, return index of match
			} else {
				++toFindIndex;
			}
			l = toFind.charAt(toFindIndex);// set character to find
			if (!caseSensitive) {
				l = Character.toLowerCase(l);
			}
		}
		return -1;
	}

	/**
	 * IndexOf method. Instead of String indexOf this can be set to be case insensitive! No more converting whole String to different case. This is same as {@link #indexOf(String, boolean, int, String)} with caseSensitive = false and from = 0 
	 * 
	 * @param context
	 *            text to search in
	 * @param caseSensitive
	 *            true if search is case sensitive, false if search is case insensitive
	 * @param toFind
	 *            string to find
	 * @return starting index of string toFind in context
	 */
	public static int indexOf(String context, String toFind) {
		return indexOf(context, false, 0, toFind);
	}
	
	/**
	 * Insert string into other string at specified index from left
	 * @param orig original string
	 * @param toInsert string to insert
	 * @param index index where to insert string from left
	 * @return
	 */
	public static String insertLeft(String orig, String toInsert, int index) {
		Require.require(index <= orig.length(), index, "index", "Index must be in range of original string!");
		StringBuilder sb = new StringBuilder(orig.length() + toInsert.length());
		if(index > 0) {
			sb.append(orig, 0, index);
		}
		sb.append(toInsert);
		if(index < orig.length()) {
			sb.append(orig, index, orig.length());
		}
		return sb.toString();
	}
	
	/**
	 * Insert string into other string at specified index from right
	 * @param orig original string
	 * @param toInsert string to insert
	 * @param index index where to insert string from right
	 * @return
	 */
	public static String insertRight(String orig, String toInsert, int index) {
		StringBuilder sb = new StringBuilder(orig.length() + toInsert.length());
		if(index < orig.length()) {
			sb.append(orig, 0, orig.length() - index);
		}
		sb.append(toInsert);
		if(index > 0) {
			sb.append(orig, orig.length() - index, orig.length());
		}
		return sb.toString();
	}
	
	/**
	 * Creates substring from left with specified maximum length if original string length is longer than maximum length. Otherwise returns original string 
	 * @param orig original string to trim from left
	 * @param maxLength maximum length of trimmed string
	 * @return substring from left with specified maximum length if original string length is longer than maximum length. Otherwise returns original string
	 */
	public static String cutFromLeft(String orig, int maxLength) {
		if(orig == null) {
			return null;
		}
		if(orig.length() <= maxLength) {
			return orig;
		}
		return orig.substring(0, maxLength);
	}
	
	/**
	 * Creates substring from right with specified maximum length if original string length is longer than maximum length. Otherwise returns original string 
	 * @param orig original string to trim from right
	 * @param maxLength maximum length of trimmed string
	 * @return substring from right with specified maximum length if original string length is longer than maximum length. Otherwise returns original string
	 */
	public static String cutFromRight(String orig, int maxLength) {
		if(orig == null) {
			return null;
		}
		if(orig.length() <= maxLength) {
			return orig;
		}
		return orig.substring(orig.length() - maxLength, orig.length());
	}
}
