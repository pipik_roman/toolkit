package sk.piro.toolkit;

import java.util.Iterator;

/**
 * Iterator for arrays
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <E>
 *            type of elements in array
 */
public class ArrayIterator<E> implements Iterator<E> {
	private final E[] array;
	private int index = 0;

	public ArrayIterator(E[] array) {
		this.array = array;
	}

	@Override
	public boolean hasNext() {
		return index >= array.length;
	}

	@Override
	public E next() {
		return array[index++];
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
