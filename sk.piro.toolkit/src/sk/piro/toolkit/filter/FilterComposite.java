package sk.piro.toolkit.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Filter for composition of multiple filters. Filter order is preserved so first filters should be fastest to
 * achieve effectivity of filtering!
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Element>
 *            Elements to filter
 */
abstract public class FilterComposite<Element> implements Filter<Element>{
	protected List<Filter<Element>> filters;

	/**
	 * Use this constructor to create mutable Composite Filter. Filters can be added through {@link #register(Filter)} method
	 */
	public FilterComposite() {
		this.filters = new ArrayList<Filter<Element>>();
	}

	public FilterComposite(Filter<Element> ... filters) {
		this();
		for(Filter<Element> filter : filters) {
			this.filters.add(filter);
		}
	}
	
	/**
	 * Use this constructor to create immutable Composite Filter.
	 * 
	 * @param filters
	 *            filters to use
	 */
	public FilterComposite(Iterable<Filter<Element>> filters) {
		this();
		for (Filter<Element> filter : filters) {
			this.filters.add(filter);
		}
	}

	/**
	 * Add filter to this composite filter
	 * 
	 * @param filter
	 *            filter to add
	 * @throws UnsupportedOperationException
	 *             if this composite filter is immutable and no filters can be added
	 */
	public void add(final Filter<Element> filter) {
		this.filters.add(filter);
	}
}
