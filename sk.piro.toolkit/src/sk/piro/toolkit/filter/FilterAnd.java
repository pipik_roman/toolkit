package sk.piro.toolkit.filter;
/**
 * Composite filter accepting elements only when all subfilters accept element
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 * @param <Element>
 */
public class FilterAnd<Element> extends FilterComposite<Element> {

	
	public FilterAnd() {
		super();
	}

	public FilterAnd(Filter<Element>... filters) {
		super(filters);
	}

	public FilterAnd(Iterable<Filter<Element>> filters) {
		super(filters);
	}

	@Override
	public boolean accept(Element element) {
		for (Filter<Element> filter : filters) {
			if (!filter.accept(element)) { return false; }
		}
		return true;
	}
}
