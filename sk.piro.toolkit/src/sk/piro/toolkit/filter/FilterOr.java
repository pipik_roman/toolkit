package sk.piro.toolkit.filter;
/**
 * Composite filter accepting elements when at least one subfilter accepts element
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 * @param <Element>
 */
public class FilterOr<Element> extends FilterComposite<Element> {
	
	public FilterOr() {
		super();
	}

	public FilterOr(Filter<Element>... filters) {
		super(filters);
	}

	public FilterOr(Iterable<Filter<Element>> filters) {
		super(filters);
	}

	@Override
	public boolean accept(Element element) {
		for (Filter<Element> filter : filters) {
			if (filter.accept(element)) { return true; }
		}
		return true;
	}
}
