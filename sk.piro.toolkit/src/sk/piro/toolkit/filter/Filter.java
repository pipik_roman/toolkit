/**
 * 
 */
package sk.piro.toolkit.filter;


/**
 * Interface for implementation of filter. Filter is implementing Observer interface so it can be used as Observer for {@link FilterComposite}
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Element>
 *            type of elements to filter
 */
public interface Filter<Element>{

	/**
	 * Method implementing filter decisions
	 * 
	 * @param element
	 *            element to filter or not
	 * @return true if element is accepted (not filtered out), false to ignore element (filter out)
	 */
	public boolean accept(Element element);
}
