package sk.piro.toolkit;

import java.io.Serializable;

/**
 * Representation of valid email address. Contains help methods for validation, accessing user and domain parts
 * 
 * @author Roman Pipík, www.piro.sk, pipik.roman@gmail.com
 */
public class EmailAddress implements Serializable, Cloneable<EmailAddress>, CharSequence {
	private static final long serialVersionUID = 1L;

	/**
	 * String representing official email regex for RFC822
	 */
	public static final String EMAIL_OFFICIAL_RFC2822_STRING = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

	/**
	 * String representing advanced email regex.
	 */
	public static final String EMAIL_ADVANCED_STRING = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

	/**
	 * String representing simple email regex
	 */
	public static final String EMAIL_SIMPLE_STRING = "[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4}";

	/**
	 * At character
	 */
	public static final char AT = '@';

	/**
	 * String representation of mail
	 */
	private String mail;

	/**
	 * GWT serialization constructor!
	 */
	private EmailAddress() {
		//For GWT serialization!
	}
	
	/**
	 * Create email fomr username and domain adding @
	 * @param username
	 * @param domain
	 */
	public EmailAddress(String username, String domain) {
		this(username + AT + domain);
	}

	/**
	 * Create mail from text. Email validity is asserted.
	 * 
	 * @param mail
	 *            text representation of mail
	 * @throws IllegalArgumentException
	 *             if mail is invalid
	 */
	public EmailAddress(String mail) {
		this();
		assert isValidMail(mail) : "Invalid mail: " + mail
				+ "! If you are using this class you should be sure of email validity, or test through Email.isValidEmail(String)";
		this.mail = mail;
	}

	/**
	 * Returns true if text is valid e-mail address
	 * 
	 * @param text
	 *            text to test
	 * @return true if text is valid e-mail address
	 */
	public static boolean isValidMail(String text) {
		if (text.matches(EMAIL_SIMPLE_STRING)) return true;
		if (text.matches(EMAIL_ADVANCED_STRING)) return true;
		if (text.matches(EMAIL_OFFICIAL_RFC2822_STRING)) return true;
		return false;
	}

	/**
	 * Returns user name part of email
	 * 
	 * @return user name part of email
	 */
	public String getUsername() {
		return this.mail.substring(0, this.mail.indexOf(AT));
	}

	/**
	 * Returns domain name part of email
	 * 
	 * @return domain name part of email
	 */
	public String getDomain() {
		return this.mail.substring(this.mail.indexOf(AT) + 1);
	}

	@Override
	public String toString() {
		return this.mail;
	}

	@Override
	public final EmailAddress clone() {
		try {
			return (EmailAddress) super.clone();
		}
		catch (CloneNotSupportedException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		EmailAddress other = (EmailAddress) obj;
		return this.mail.equals(other.mail);
	}

	/**
	 * Compares two email's with ignoring specified character
	 * <p>
	 * <b>Note:</b>Can be used for comparing email's like Gmail where dots are ignored !
	 * </p>
	 * 
	 * @param email
	 *            email to compare
	 * @param ignore
	 *            character to ignore when comparing
	 * @return true if email's are equal after removing ignored character
	 */
	public boolean equals(EmailAddress email, char ignore) {
		return this.mail.replace(ignore + "", "").equals(email.toString().replace(ignore + "", ""));
	}

	@Override
	public int hashCode() {
		return mail.hashCode();
	}

	@Override
	public char charAt(int index) {
		return this.mail.charAt(index);
	}

	@Override
	public int length() {
		return this.mail.length();
	}

	@Override
	public CharSequence subSequence(int begin, int end) {
		return this.mail.subSequence(begin, end);
	}
}
