package sk.piro.toolkit;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import junit.framework.TestCase;

public class StringUtilsTest extends TestCase{
	@Test
	public void testReplaceAllChars() {
		String result = StringUtils.replaceAllChars("abcdefgh", new char[] {'a','c','e','g'},new char[] {'A','C','E','G'} );
		assertEquals(result, "AbCdEfGh");
		result = StringUtils.replaceAllChars("abcdefgh", '_','a','c','e','g' );
		assertEquals(result, "_b_d_f_h");
	}
	
	@Test
	public void testInsertLeft() {
		String result = StringUtils.insertLeft("123456","ABC", 0);
		assertEquals("ABC123456", result);
		result = StringUtils.insertLeft("123456","ABC", 1);
		assertEquals("1ABC23456", result);
		result = StringUtils.insertLeft("123456","ABC", 5);
		assertEquals("12345ABC6", result);
		result = StringUtils.insertLeft("123456","ABC", 6);
		assertEquals("123456ABC", result);
		
	}
	
	@Test
	public void testInsertRight() {
		String result = StringUtils.insertRight("123456","ABC", 0);
		assertEquals("123456ABC", result);
		result = StringUtils.insertRight("123456","ABC", 1);
		assertEquals("12345ABC6", result);
		result = StringUtils.insertRight("123456","ABC", 5);
		assertEquals("1ABC23456", result);
		result = StringUtils.insertRight("123456","ABC", 6);
		assertEquals("ABC123456", result);
		
	}
	
	@Test
	public void testParseBoolean() {
		assertEquals(true, StringUtils.parseBoolean("true"));
		assertEquals(true, StringUtils.parseBoolean("1"));
		assertEquals(true, StringUtils.parseBoolean("t"));
		assertEquals(false, StringUtils.parseBoolean(""));
		assertEquals(false, StringUtils.parseBoolean("anything"));
		assertEquals(false, StringUtils.parseBoolean("false"));
		assertEquals(false, StringUtils.parseBoolean("0"));
		assertEquals(false, StringUtils.parseBoolean("f"));
		assertEquals(false, StringUtils.parseBoolean("a"));
		assertEquals(false, StringUtils.parseBoolean("y"));
		assertEquals(false, StringUtils.parseBoolean("yes"));
		assertEquals(true, StringUtils.parseBoolean("a",LocaleUtils.LANGUAGE_SK));
		assertEquals(true, StringUtils.parseBoolean("y",LocaleUtils.LANGUAGE_EN));
		assertEquals(true, StringUtils.parseBoolean("yes",LocaleUtils.LANGUAGE_EN));
	}
	
	@Test 
	public void testIterableToCsv(){
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("one", Integer.valueOf(1));
		map.put("two", Integer.valueOf(2));
		System.err.println(StringUtils.csvString(map.entrySet()));
	}
	
	@Test
	public void testCutFromLeft() {
		String orig = "1234567890";
		String result = StringUtils.cutFromLeft(orig, 5);
		assertEquals("12345", result);
		orig = null;
		result = StringUtils.cutFromLeft(orig, 5);
		assertEquals(null, result);
		orig = "123";
		result = StringUtils.cutFromLeft(orig, 5);
		assertEquals("123", result);
		orig = "12345";
		result = StringUtils.cutFromLeft(orig, 5);
		assertEquals("12345", result);
	}
	
	@Test
	public void testCutFromRight() {
		String orig = "1234567890";
		String result = StringUtils.cutFromRight(orig, 5);
		assertEquals("67890", result);
		orig = null;
		result = StringUtils.cutFromRight(orig, 5);
		assertEquals(null, result);
		orig = "123";
		result = StringUtils.cutFromRight(orig, 5);
		assertEquals("123", result);
		orig = "12345";
		result = StringUtils.cutFromRight(orig, 5);
		assertEquals("12345", result);
	}
}
