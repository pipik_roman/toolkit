package sk.piro.toolkit;

import org.junit.Test;

import junit.framework.TestCase;

public class RequireTest extends TestCase{

	@Test
	public void testIsSame() {
		try{
			Require.isSame(1, 1, "number");
		}
		catch(IllegalArgumentException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		try{
			Require.isSame(1, 2, "number");
			fail("Require.isSame(1, 2, ...); should fail!");
		}
		catch(IllegalArgumentException e) {
			assertEquals("Field 'number' of type 'java.lang.Integer' is not same as expected value '1' '2'!", e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testIsOneOf() {
		try{
			Require.isOneOf(1, "number",null,1,2,3);
		}
		catch(IllegalArgumentException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		try{
			Require.isOneOf(3, "number",1,null,2,3);
		}
		catch(IllegalArgumentException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		try{
			Require.isOneOf(1, "number",2,3,4);
			fail("Require.isOneOf(1, number, 2,3,4); should fail!");
		}
		catch(IllegalArgumentException e) {
			assertEquals("Field 'number' of type 'java.lang.Integer'  is not one of expected values [2, 3, 4], but is '1'!", e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Test
	public void testIsOneOfWithNulls() {
		try{
			Require.isOneOf(null, "number",1,null,3);
		}
		catch(IllegalArgumentException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		try{
			Require.isOneOf(null, "number",2,3,4);
			fail("Require.isOneOf(null,number, 2,3,4); should fail!");
		}
		catch(IllegalArgumentException e) {
			assertEquals("Field 'number' of type '?'  is not one of expected values [2, 3, 4], but is 'null'!", e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRequireWithDescription() {
		try {
			Require.require(false, "TST", "TST_FIELD_NAME", "TST_PARAM", 1, ' ', "TST_PARAM_", 2);
			fail("Require.require(false, TST, TST_FIELD_NAME, TST_PARAM, 1, ' ', TST_PARAM_, 2); should fail!");
		} catch (IllegalArgumentException e) {
			assertEquals("Field 'TST_FIELD_NAME' of type 'java.lang.String' has invalid value 'TST'! TST_PARAM1 TST_PARAM_2", e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRequireWillNotCreateStringsWhenValid() {
		Object o = new Object() {
			@Override
			public String toString() {
				fail("toString cannot be called on object since requirement is valid!");
				throw new AssertionError();
			};
		};
		try {
			Require.require(true, "TST", "TST_FIELD_NAME", "TST_PARAM", o);
		} catch (IllegalArgumentException e) {
			fail("Requirement is valid so this should not throw!");
			e.printStackTrace();
		}
	}
}
